{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE EmptyDataDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

-- base
import Control.Applicative (liftA2, (<|>))
import Control.Arrow (second)
import Control.Monad (ap, join)
import Data.Bifunctor (bimap)
import Data.Bool (bool)
import Data.Data (Data, Typeable)
import Data.Foldable (fold)
import Data.Functor.Classes
  ( Eq1(..)
  , Read1 (..) , readsData, readsUnaryWith
  , Show1 (..), showsUnaryWith
  )
import Data.Void (Void, absurd)
import GHC.Generics (Generic)
import Prelude hiding (head, tail, init, last)

-- bound
import Bound (Scope (Scope), (>>>=), instantiate1, unscope, instantiate, Var (F, B), fromScope)
import Bound.Var (unvar)

-- data-fix
import Data.Fix (Mu (Mu), foldMu, unfoldMu)

-- kan-extensions
import Data.Functor.Kan.Ran (Ran)

-- text
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.IO as TextIO
import Data.Maybe (isJust)
import Data.Functor.Const (Const(..))

data Grade = None | One | Maybe | Ton | Some | Many
  deriving (Eq, Show, Read)

data LevelF r
  = LevelFZero
  | LevelFSuccessor r
  deriving (Eq, Ord, Show, Read, Functor)

instance Eq1 LevelF where
  liftEq _ LevelFZero LevelFZero = True
  liftEq eq (LevelFSuccessor x) (LevelFSuccessor y) = eq x y
  liftEq _ _ _ = False

instance Read1 LevelF where
  liftReadsPrec rp _rl = readsData $
    const (\tok -> [(LevelFZero, []) | "LevelFZero" == tok]) <>
    readsUnaryWith rp "LevelFSuccessor" LevelFSuccessor

instance Show1 LevelF where
  liftShowsPrec sp _sl p = \case
    LevelFZero -> ("LevelFZero" ++)
    LevelFSuccessor x -> showsUnaryWith sp "LevelFSuccessor" p x

type Level = Mu LevelF

levelZero :: Level
levelZero = Mu ($ LevelFZero)

levelSuccessor :: Level -> Level
levelSuccessor (Mu x) = Mu \f -> f (LevelFSuccessor $ x f)

foldLevel :: r -> (r -> r) -> Level -> r
foldLevel z s = foldMu f
 where
  f LevelFZero = z
  f (LevelFSuccessor x) = s x

levelUnfold :: (r -> Maybe r) -> r -> Level
levelUnfold s = unfoldMu u
 where
  u = maybe LevelFZero LevelFSuccessor . s

data Term identifier
  = Var identifier
  | Type Level
  | ArrowTerm (Arrow identifier)
  | LambdaTerm (Lambda identifier)
  | ApplicationTerm (Application identifier)
  | TensorTerm (Tensor identifier)
  | PairTerm (Pair identifier)
  | UnpairTerm (Unpair identifier)
  | GradedTerm (Graded identifier)
  | Promote (Term identifier)
  | DemoteTerm (Demote identifier)
  deriving (Functor, Foldable, Traversable, Typeable, Generic)

instance Applicative Term where
  pure = Var
  (<*>) = ap

instance Monad Term where
  return = Var
  Var x >>= f = f x
  Type l >>= _ = Type l
  ArrowTerm a >>= f = ArrowTerm a { domain = domain a >>= f, codomain = codomain a >>>= f }
  LambdaTerm l >>= f = LambdaTerm l { lambdaBody = lambdaBody l >>>= f }
  ApplicationTerm a >>= f = ApplicationTerm MkApplication { function = function a >>= f, argument = argument a >>= f }
  TensorTerm t >>= f = TensorTerm t { indexSet = indexSet t >>= f, property = property t >>>= f }
  PairTerm p >>= f = PairTerm MkPair { witness = witness p >>= f, proof = proof p >>= f }
  UnpairTerm u >>= f = UnpairTerm u { pairTerm = pairTerm u >>= f, unpairBody = unpairBody u >>>= f }
  GradedTerm g >>= f = GradedTerm g { set = set g >>= f }
  Promote p >>= f = Promote $ p >>= f
  DemoteTerm d >>= f = DemoteTerm d { demoteTerm = demoteTerm d >>= f, demoteBody = demoteBody d >>>= f}

data Arrow identifier = MkArrow
  { arrowBinder :: Text
  , bodyGrade :: Grade
  , codomainGrade :: Grade
  , domain :: Term identifier
  , codomain :: Scope () Term identifier
  } deriving (Functor, Foldable, Traversable, Typeable, Generic)

data Lambda identifier = MkLambda
  { lambdaBinder :: Text
  , lambdaBody :: Scope () Term identifier
  } deriving (Functor, Foldable, Traversable, Typeable, Generic)

data Application identifier = MkApplication
  { function :: Term identifier
  , argument :: Term identifier
  } deriving (Functor, Foldable, Traversable, Typeable, Generic)

data Tensor identifier = MkTensor
  { tensorBinder :: Text
  , witnessGrade :: Grade
  , indexSet :: Term identifier
  , property :: Scope () Term identifier
  } deriving (Functor, Foldable, Traversable, Typeable, Generic)

data Pair identifier = MkPair
  { witness :: Term identifier
  , proof :: Term identifier
  } deriving (Functor, Foldable, Traversable, Typeable, Generic)

data Unpair identifier = MkUnpair
  { witnessBinder :: Text
  , proofBinder :: Text
  , pairTerm :: Term identifier
  , unpairBody :: Scope Bool Term identifier
  } deriving (Functor, Foldable, Traversable, Typeable, Generic)

data Graded identifier = MkGraded
  { grade :: Grade
  , set :: Term identifier
  } deriving (Functor, Foldable, Traversable, Typeable, Generic)

data Demote identifier = MkDemote
  { demoteBinder :: Text
  , demoteTerm :: Term identifier
  , demoteBody :: Scope () Term identifier
  } deriving (Functor, Foldable, Traversable, Typeable, Generic)

data BadTerm
  = ApplyingNonFunction
  | UnpairingNonPair
  | DemotingNonPromote
  deriving (Eq, Show, Read, Typeable, Data, Generic)

data SmallStep
  = BetaFunction
  | BetaTensor
  | BetaGraded
  | PairWitness SmallStep
  | PairProof SmallStep
  | ApplicationFunction SmallStep
  | UnpairPair SmallStep
  | DemotePromote SmallStep
  | StepGraded SmallStep
  | ArrowDomain SmallStep
  | ArrowCodomain SmallStep
  | TensorIndex SmallStep
  | TensorProperty SmallStep
  | StepPromote SmallStep -- Missing from paper!?
 deriving (Eq, Show, Read, Typeable, Data, Generic)

smallStep :: Term identifier -> Either BadTerm (Maybe (SmallStep, Term identifier))
smallStep Var{} = Right Nothing
smallStep Type{} = Right Nothing
smallStep (ArrowTerm a) = liftA2 (<|>) stepDomain stepCodomain
 where
  stepDomain =
    fmap (\(step, newDomain) -> (ArrowDomain step, ArrowTerm a { domain = newDomain })) <$>
      smallStep (domain a)
  stepCodomain =
    fmap (\(step, newCodomain) -> (ArrowCodomain step, ArrowTerm a { codomain = newCodomain })) <$>
      smallStepScope (codomain a)
smallStep LambdaTerm{} = Right Nothing
smallStep (ApplicationTerm a) = case f of
  Var{} -> Right Nothing
  LambdaTerm MkLambda{..} ->
    Right $ Just (BetaFunction, instantiate1 (argument a) lambdaBody)
  ApplicationTerm{} -> cut
  UnpairTerm{} -> cut
  DemoteTerm{} -> cut
  _ -> Left ApplyingNonFunction
 where
  f = function a
  cut =
    fmap (\(step, newFunction) -> (ApplicationFunction step, ApplicationTerm a { function = newFunction })) <$>
      smallStep f
smallStep (TensorTerm t) = liftA2 (<|>) stepIndex stepProperty
 where
  stepIndex =
    fmap (\(step, newIndex) -> (TensorIndex step, TensorTerm t { indexSet = newIndex })) <$>
      smallStep (indexSet t)
  stepProperty =
    fmap (\(step, newProperty) -> (TensorProperty step, TensorTerm t { property = newProperty })) <$>
      smallStepScope (property t)
smallStep (PairTerm p) = liftA2 (<|>) stepWitness stepProof
 where
  stepWitness =
    fmap (\(step, newWitness) -> (PairWitness step, PairTerm p { witness = newWitness })) <$>
      smallStep (witness p)
  stepProof =
    fmap (\(step, newProof) -> (PairProof step, PairTerm p { proof = newProof })) <$>
      smallStep (proof p)
smallStep (UnpairTerm u) = case p of
  Var{} -> Right Nothing
  PairTerm MkPair{..} ->
    Right $ Just (BetaTensor, instantiate (bool witness proof) $ unpairBody u)
  ApplicationTerm{} -> cut
  UnpairTerm{} -> cut
  DemoteTerm{} -> cut
  _ -> Left UnpairingNonPair
 where
  p = pairTerm u
  cut =
    fmap (\(step, newPair) -> (UnpairPair step, UnpairTerm u { pairTerm = newPair })) <$>
      smallStep p
smallStep (GradedTerm g) =
  fmap (\(step, newSet) -> (StepGraded step, GradedTerm g { set = newSet})) <$> smallStep (set g)
smallStep (Promote p) =
  fmap (bimap StepPromote Promote) <$> smallStep p
smallStep (DemoteTerm d) = case t of
  Var{} -> Right Nothing
  Promote t2 -> Right $ Just (BetaGraded, instantiate1 t2 $ demoteBody d)
  ApplicationTerm{} -> cut
  UnpairTerm{} -> cut
  DemoteTerm{} -> cut
  _ -> Left DemotingNonPromote
 where
  t = demoteTerm d
  cut =
    fmap (\(step, newTerm) -> (DemotePromote step, DemoteTerm d { demoteTerm = newTerm })) <$>
      smallStep t

smallStepScope :: Scope bound Term free -> Either BadTerm (Maybe (SmallStep, Scope bound Term free))
smallStepScope = fmap (fmap (second Scope)) . smallStep . unscope

render :: (identifier -> Text) -> Term identifier -> Text
render renderIdent = r (const renderIdent) False
 where
  r :: (Bool -> id2 -> Text) -> Bool -> Term id2 -> Text
  r renderFree = r2
   where
    r2 needTerm = r3
     where
      addNeededTerm :: Text -> Text
      addNeededTerm x = bool x ("(" <> x <> ")") needTerm
      r3 = \case
        Var x -> renderFree needTerm x
        Type l -> "Type " <> renderShow (integerLevel l)
        ArrowTerm MkArrow{..} ->
          fold
            ["("
            , arrowBinder
            , " <"
            , renderShow bodyGrade
            , ","
            , renderShow codomainGrade
            , "> "
            , r2 False domain
            , ") -> "
            , rs (const arrowBinder) True codomain
            ]
        LambdaTerm MkLambda{..} ->
          addNeededTerm $ "\\" <> lambdaBinder <> "." <> rs (const lambdaBinder) False lambdaBody
        ApplicationTerm MkApplication{..} -> r2 True function <> " " <> r3 argument
        TensorTerm MkTensor{..} ->
          fold
            [ "("
            , tensorBinder
            , " "
            , renderShow witnessGrade
            , " "
            , r2 False indexSet
            , ") * "
            , rs (const tensorBinder) True property
            ]
        PairTerm MkPair{..} -> "(" <> r2 False witness <> "," <> r2 False proof <> ")"
        UnpairTerm MkUnpair{..} ->
          addNeededTerm $
            fold
              [ "let ("
              , witnessBinder
              , ","
              , proofBinder
              , ") = "
              , r2 False pairTerm
              , " in "
              , rs (bool witnessBinder proofBinder) False unpairBody
              ]
        GradedTerm MkGraded{..} -> "Graded " <> renderShow grade <> " " <> r3 set
        Promote p -> "Promote " <> r3 p
        DemoteTerm MkDemote{..} ->
          addNeededTerm $
            fold
              [ "let Promote "
              , demoteBinder
              , " = "
              , r2 False demoteTerm
              , " in "
              , rs (const demoteBinder) False demoteBody
              ]
    rs renderBound needTop = r (flip $ unvar (const . renderBound) (flip renderFree)) needTop . fromScope

renderShow :: Show a => a -> Text
renderShow = Text.pack . show

integerLevel :: Level -> Integer
integerLevel = foldLevel 0 succ

termId :: Term identifier
termId = LambdaTerm MkLambda
  { lambdaBinder = Text.pack "x"
  , lambdaBody = Scope . Var $ B ()
  }

termSelfApply :: Term identifier
termSelfApply = LambdaTerm MkLambda
  { lambdaBinder = Text.pack "x"
  , lambdaBody = Scope $ ApplicationTerm MkApplication
    { function = Var $ B ()
    , argument = Var $ B ()
    }
  }

exampleTerm :: Term Void
exampleTerm = ApplicationTerm MkApplication
  { function = LambdaTerm MkLambda
    { lambdaBinder = Text.pack "pp"
    , lambdaBody = Scope $ UnpairTerm MkUnpair
      { witnessBinder = Text.pack "f"
      , proofBinder = Text.pack "x"
      , pairTerm = DemoteTerm MkDemote
        { demoteBinder = Text.pack "d"
        , demoteTerm = Var $ B ()
        , demoteBody = Scope . Var $ B ()
        }
      , unpairBody = Scope . Var $ B True
      }
    }
  , argument = PairTerm MkPair
    { witness = termId
    , proof = Promote termSelfApply
    }
  }

termCoerceType :: Term identifier
termCoerceType = ArrowTerm MkArrow
  { arrowBinder = Text.pack "A"
  , bodyGrade = None
  , codomainGrade = One
  , domain = Type levelZero
  , codomain = Scope $ ArrowTerm MkArrow
    { arrowBinder = Text.pack "B"
    , bodyGrade = None
    , codomainGrade = One
    , domain = Type levelZero
    , codomain = Scope $ ArrowTerm MkArrow
      { arrowBinder = Text.pack "x"
      , bodyGrade = One
      , codomainGrade = None
      , domain = Var . F . Var $ B ()
      , codomain = Scope $ Var . F . Var $ B ()
      }
    }
  }

weakenTerm :: Term identifier -> Scope a Term identifier
weakenTerm = Scope . Var . F

strengthenTerm :: Scope a Term identifier -> Maybe (Term identifier)
strengthenTerm = fmap join . traverse debind . unscope
 where
   debind (B _) = fail "Uses bound!"
   debind (F t) = pure t

type Context a = forall fr. fr Void -> (forall i. fr i -> Term i -> fr (Var () i)) -> fr a

newtype AContext a = MkContext { unContext :: Context a }

emptyContext :: Context Void
emptyContext fvoid _fvar = fvoid

snocContext :: Context identifier -> Term identifier -> Context (Var () identifier)
snocContext init last fvoid fvar = fvar (init fvoid fvar) last

foldContext :: f Void -> (forall i. f i -> Term i -> f (Var () i)) -> Context a -> f a
foldContext fvoid fvar x = x fvoid fvar

foldAContext :: f Void -> (forall i. f i -> Term i -> f (Var () i)) -> AContext a -> f a
foldAContext void var x = foldContext void var (unContext x)

newtype RunContext identifier = MkRunContext { unRunContext :: identifier -> Term identifier }

runContext :: Context identifier -> identifier -> Term identifier
runContext ctx = unRunContext (ctx nil snoc)
 where
  nil = MkRunContext absurd
  snoc :: forall i. RunContext i -> Term i -> RunContext (Var () i)
  snoc init last = MkRunContext (unvar (const (F <$> last)) (fmap F . unRunContext init))

isEmptyContext :: Context identifier -> Bool
isEmptyContext c = getConst $ c (Const False) (\_ _ -> Const True)

wellFormed :: Context identifier -> Bool
wellFormed c =
  getConst $ c (Const True) (\i l -> Const $ getConst i && ({- infer l i == Type? -} const False l))

typeCheck ::
  (identifier -> (Term identifier, [Grade])) ->
  -- | "term"
  Term identifier ->
  -- | "type"
  Term identifier ->
  Maybe ([Grade], [Grade])
typeCheck _ (Var x) _ = Nothing
typeCheck _ (Type l) (Type sl) =
  if levelSuccessor l == sl then Just (repeat None, repeat None) else Nothing
typeCheck _ _ _ = Nothing

main :: IO ()
main = do
  print $ isEmptyContext emptyContext
  print $ isEmptyContext $ snocContext emptyContext $ Type levelZero
  loop termCoerceType
 where
  loop t = do
    TextIO.putStrLn $ render absurd t
    case smallStep t of
      Left err -> putStr "Fatal: " >> print err
      Right Nothing -> pure ()
      Right (Just (step, t2)) -> print step >> loop t2
