open import Agda.Builtin.Bool
open import Agda.Builtin.String using (primStringEquality)
  renaming (String to Identifier)
open import Agda.Builtin.Equality using (refl) renaming(_≡_ to Eq)
open import Agda.Builtin.Unit renaming (⊤ to Unit)
open import Agda.Builtin.Sigma renaming (Σ to Sigma)

open import Grade

data Void : Set where

Not : Set -> Set
Not x = Void -> x

data Level : Set where
  zero : Level
  succ : Level -> Level

lub : Level -> Level -> Level
lub zero zero = zero
lub zero p@(succ _) = p
lub p@(succ _) zero = p
lub (succ l) (succ r) = succ (lub l r)

LTE : Level -> Level -> Set
LTE l r = Eq (lub l r) r

zero-lub-left-neutral-x : (x : Level) -> Eq (lub zero x) x
zero-lub-left-neutral-x zero = refl
zero-lub-left-neutral-x (succ _) = refl

zero-lub-left-neutral : {x : Level} -> Eq (lub zero x) x
zero-lub-left-neutral {x} = zero-lub-left-neutral-x x

zero-lub-right-neutral-x : (x : Level) -> Eq (lub x zero) x
zero-lub-right-neutral-x zero = refl
zero-lub-right-neutral-x (succ _) = refl

zero-lub-right-neutral : {x : Level} -> Eq (lub x zero) x
zero-lub-right-neutral {x} = zero-lub-right-neutral-x x

zero-min : {x : Level} -> LTE zero x
zero-min = zero-lub-left-neutral

data Term : Set where
  var : Identifier -> Term
  type : Level -> Term
  arrow : Identifier -> Grade -> Grade -> Term -> Term -> Term
  lambda : Identifier -> Term -> Term
  apply : Term -> Term -> Term
  tensor : Identifier -> Grade -> Term -> Term -> Term
  pair : Term -> Term -> Term
  unpair : Identifier -> Identifier -> Term -> Term -> Term
  graded : Grade -> Term -> Term
  promote : Term -> Term
  demote : Identifier -> Term -> Term -> Term

_||_ : Bool -> Bool -> Bool
true || _ = true
_fls || x = x

substitute : Term -> Identifier -> Term -> Term
substitute t@(var x) y v = c (primStringEquality x y)
 where
  c : Bool -> Term
  c true = v
  c false = t
substitute t@(type _) _ _ = t
substitute (arrow x s t d c) y v =
  arrow x s t (substitute d y v) (cs (primStringEquality x y))
 where
  cs : Bool -> Term
  cs true = c
  cs false = substitute c y v
substitute t@(lambda x b) y v = c (primStringEquality x y)
 where
  c : Bool -> Term
  c true = t
  c false = lambda x (substitute b y v)
substitute (apply f x) y v = apply (substitute f y v) (substitute x y v)
substitute (tensor x s w p) y v =
  tensor x s (substitute w y v) (c (primStringEquality x y))
 where
  c : Bool -> Term
  c true = p
  c false = substitute p y v
substitute (pair f s) y v = pair (substitute f y v) (substitute s y v)
substitute (unpair x1 x2 p b) y v =
  unpair x1 x2 p (c (primStringEquality x1 y || primStringEquality x2 y))
 where
  c : Bool -> Term
  c true = b
  c false = substitute b y v
substitute (graded s x) y v = graded s (substitute x y v)
substitute (promote x) y v = promote (substitute x y v)
substitute (demote x p b) y v = demote x p (c (primStringEquality x y))
 where
  c : Bool -> Term
  c true = b
  c false = substitute b y v

data GradeVector : Set where
  empty : GradeVector
  snoc : GradeVector -> Grade -> GradeVector

vector-add : GradeVector -> GradeVector -> GradeVector
vector-add empty _ = empty
vector-add _ empty = empty
vector-add (snoc l x) (snoc r y) = snoc (vector-add l r) (add x y)

vector-scale : Grade -> GradeVector -> GradeVector
vector-scale x = v-s-x
 where
  v-s-x : GradeVector -> GradeVector
  v-s-x empty = empty
  v-s-x (snoc i y) = snoc (v-s-x i) (multiply x y)

zeros-like : GradeVector -> GradeVector
zeros-like empty = empty
zeros-like (snoc v _) = snoc (zeros-like v) none

cong : {a b : Set} {f : a -> b} {x y : a} -> Eq x y -> Eq (f x) (f y)
cong refl = refl

cong-f : {a b : Set} (f : a -> b) -> {x y : a} -> Eq x y -> Eq (f x) (f y)
cong-f _ = cong

zeros-like-scale-none-v : (v : GradeVector) -> Eq (zeros-like v) (vector-scale none v)
zeros-like-scale-none-v empty = refl
zeros-like-scale-none-v (snoc i _) = cong-f s-n (zeros-like-scale-none-v i)
 where
  s-n : GradeVector -> GradeVector
  s-n x = snoc x none

grades-splice : GradeVector -> Grade -> GradeVector -> GradeVector
grades-splice init s empty = snoc init s
grades-splice init s (snoc tail r) = snoc (grades-splice init s tail) r

data ContextGradeVector : Set where
  empty : ContextGradeVector
  snoc : ContextGradeVector -> GradeVector -> ContextGradeVector

zeros-like-context : ContextGradeVector -> GradeVector
zeros-like-context empty = empty
zeros-like-context (snoc v _) = snoc (zeros-like-context v) none

context-grades-splice :
  ContextGradeVector -> GradeVector -> ContextGradeVector -> ContextGradeVector
context-grades-splice init sigma empty = snoc init sigma
context-grades-splice init sigma1 (snoc tail sigma2) =
  snoc (context-grades-splice init sigma1 tail) sigma2

grades-length : ContextGradeVector -> Level
grades-length empty = zero
grades-length (snoc init _) = succ (grades-length init)

data Context : Set where
  empty : Context
  snoc : Context -> Identifier -> Term -> Context

context-splice : Context -> Identifier -> Term -> Context -> Context
context-splice init x a empty = snoc init x a
context-splice init x a (snoc tail y b) =
  snoc (context-splice init x a tail) y b

context-length : Context -> Level
context-length empty = zero
context-length (snoc init _ _) = succ (context-length init)

data WellFormed : ContextGradeVector -> Context -> Set
data Typing :
  ContextGradeVector -> GradeVector -> GradeVector -> Context -> Term -> Term ->
  Set
data TermEquality :
  ContextGradeVector -> GradeVector -> GradeVector -> Context ->
  Term -> Term -> Term ->
  Set
data Subtyping :
  ContextGradeVector -> GradeVector -> Context -> Term -> Term -> Set

data WellFormed where
  empty : WellFormed empty empty
  ext : {context-grades : ContextGradeVector} {set-grades : GradeVector}
     -> {context : Context} {set : Term} {level : Level}
     -> (identifier : Identifier)
     -> Typing context-grades
               set-grades
               (zeros-like set-grades)
               context
               set
               (type level)
     -> WellFormed (snoc context-grades set-grades) (snoc context identifier set)

data Subtyping where
  eq : {context-grades : ContextGradeVector} -> {types-grades : GradeVector}
    -> {context : Context} -> {subtype : Term} -> {supertype : Term}
    -> {level : Level}
    -> TermEquality context-grades
                    types-grades
                    (zeros-like types-grades)
                    context
                    subtype
                    supertype
                    (type level)
    -> Subtyping context-grades types-grades context subtype supertype
  trans : {context-grades : ContextGradeVector} -> {types-grades : GradeVector}
       -> {context : Context} -> {small medium : Term}
       -> Subtyping context-grades types-grades context small medium
       -> {large : Term}
       -> Subtyping context-grades types-grades context medium large
       -> Subtyping context-grades types-grades context small large
  type : {context-grades : ContextGradeVector} -> {context : Context}
      -> WellFormed context-grades context
      -> {small : Level} -> {large : Level}
      -> LTE small large
      -> Subtyping context-grades
                   (zeros-like-context context-grades)
                   context
                   (type small)
                   (type large)
  arrow : {context-grades : ContextGradeVector} -> {domain-grades : GradeVector}
       -> {codomain-grades : GradeVector} -> {codomain-arg-grade : Grade}
       -> {context : Context} -> {identifier : Identifier}
       -> {superdomain : Term} -> {subcodomain : Term} -> {level : Level}
       -> Typing (snoc context-grades domain-grades)
                 (snoc codomain-grades codomain-arg-grade)
                 (snoc (zeros-like codomain-grades) none)
                 (snoc context identifier superdomain)
                 subcodomain
                 (type level)
       -> {subdomain : Term}
       -> Subtyping context-grades
                    domain-grades
                    context
                    subdomain
                    superdomain
       -> {supercodomain : Term}
       -> Subtyping (snoc context-grades domain-grades)
                    (snoc codomain-grades codomain-arg-grade)
                    (snoc context identifier superdomain)
                    subcodomain
                    supercodomain
       -> (subject-grade : Grade)
       -> Subtyping context-grades
                    (vector-add domain-grades codomain-grades)
                    context
                    (arrow identifier subject-grade codomain-arg-grade superdomain subcodomain)
                    (arrow identifier subject-grade codomain-arg-grade subdomain supercodomain)
  tensor : {context-grades : ContextGradeVector} -> {type-grades : GradeVector}
        -> {property-grades : GradeVector} -> {witness-grade : Grade}
        -> {context : Context} -> {identifier : Identifier} -> {type : Term}
        -> {subproperty : Term} -> {superproperty : Term}
        -> Subtyping (snoc context-grades type-grades)
                     (snoc property-grades witness-grade)
                     (snoc context identifier type)
                     subproperty
                     superproperty
        -> Subtyping context-grades
                     (vector-add type-grades property-grades)
                     context
                     (tensor identifier witness-grade type subproperty)
                     (tensor identifier witness-grade type superproperty)
  graded : {context-grades : ContextGradeVector} -> {type-grades : GradeVector}
        -> {context : Context} -> {subtype : Term} -> {supertype : Term}
        -> Subtyping context-grades
                     type-grades
                     context
                     subtype
                     supertype
        -> (grade : Grade)
        -> Subtyping context-grades
                     type-grades
                     context
                     (graded grade subtype)
                     (graded grade supertype)

data Typing where
  type : {context-grades : ContextGradeVector} -> {context : Context}
      -> WellFormed context-grades context -> (level : Level)
      -> Typing context-grades
                (zeros-like-context context-grades)
                (zeros-like-context context-grades)
                context
                (type level)
                (type (succ level))
  var : {init-grades : ContextGradeVector} -> {grades : GradeVector}
     -> (tail-grades : ContextGradeVector)
     -> {init-context : Context} -> {identifier : Identifier} -> {type : Term}
     -> (tail-context : Context)
     -> WellFormed (context-grades-splice init-grades grades tail-grades)
                   (context-splice init-context identifier type tail-context)
     -> Eq (grades-length init-grades) (context-length init-context)
     -> Typing (context-grades-splice init-grades grades tail-grades)
               (grades-splice (zeros-like-context init-grades)
                              one
                              (zeros-like-context tail-grades))
               (grades-splice grades none (zeros-like-context tail-grades))
               (context-splice init-context identifier type tail-context)
               (var identifier)
               type
  arrow : {context-grades : ContextGradeVector} -> {domain-grades : GradeVector}
       -> {context : Context} -> {domain : Term} -> {domain-level : Level}
       -> Typing context-grades
                 domain-grades
                 (zeros-like domain-grades)
                 context
                 domain
                 (type domain-level)
       -> {codomain-context-grades : GradeVector}
       -> {codomain-arg-grade : Grade}
       -> {identifier : Identifier} -> {codomain : Term}
       -> {codomain-level : Level}
       -> Typing (snoc context-grades domain-grades)
                 (snoc codomain-context-grades codomain-arg-grade)
                 (snoc (zeros-like codomain-context-grades) none)
                 (snoc context identifier domain)
                 codomain
                 (type codomain-level)
       -> (subject-grade : Grade)
       -> Typing context-grades
                 (vector-add domain-grades codomain-context-grades)
                 (zeros-like domain-grades)
                 context
                 (arrow identifier subject-grade codomain-arg-grade domain codomain)
                 (type (lub domain-level codomain-level))
  tensor : {context-grades : ContextGradeVector} -> {set-grades : GradeVector}
        -> {context : Context} -> {set : Term} -> {set-level : Level}
        -> Typing context-grades
                  set-grades
                  (zeros-like set-grades)
                  context
                  set
                  (type set-level)
        -> {property-grades : GradeVector} -> {witness-grade : Grade}
        -> {identifier : Identifier}
        -> {property : Term} -> {property-level : Level}
        -> Typing (snoc context-grades set-grades)
                  (snoc property-grades witness-grade)
                  (snoc (zeros-like property-grades) none)
                  (snoc context identifier set)
                  property
                  (type property-level)
        -> Typing context-grades
                  (vector-add set-grades property-grades)
                  (zeros-like set-grades)
                  context
                  (tensor identifier witness-grade set property)
                  (type (lub set-level property-level))
  function : {context-grades : ContextGradeVector}
          -> {domain-grades : GradeVector}
          -> {codomain-context-grades : GradeVector}
          -> {codomain-arg-grade : Grade}
          -> {context : Context} -> {identifier : Identifier} -> {domain : Term}
          -> {codomain : Term} -> {codomain-level : Level}
          -> Typing (snoc context-grades domain-grades)
                    (snoc codomain-context-grades codomain-arg-grade)
                    (snoc (zeros-like codomain-context-grades) none)
                    (snoc context identifier domain)
                    codomain
                    (type codomain-level)
          -> {body-context-grades : GradeVector} -> {body-arg-grade : Grade}
          -> {body : Term}
          -> Typing (snoc context-grades domain-grades)
                    (snoc body-context-grades body-arg-grade)
                    (snoc codomain-context-grades codomain-arg-grade)
                    (snoc context identifier domain)
                    body
                    codomain
          -> Typing context-grades
                    body-context-grades
                    (vector-add domain-grades codomain-context-grades)
                    context
                    (lambda identifier body)
                    (arrow identifier
                           body-arg-grade
                           codomain-arg-grade
                           domain
                           codomain)
  apply : {context-grades : ContextGradeVector}
       -> {domain-grades : GradeVector}
       -> {codomain-context-grades : GradeVector}
       -> {codomain-arg-grade : Grade}
       -> {context : Context} -> {identifier : Identifier} -> {domain : Term}
       -> {codomain : Term} -> {codomain-level : Level}
       -> Typing (snoc context-grades domain-grades)
                 (snoc codomain-context-grades codomain-arg-grade)
                 (snoc (zeros-like codomain-context-grades) none)
                 (snoc context identifier domain)
                 codomain
                 (type codomain-level)
       -> {function-grades : GradeVector}
       -> {function : Term} -> {subject-grade : Grade}
       -> Typing context-grades
                 function-grades
                 (vector-add domain-grades codomain-context-grades)
                 context
                 function
                 (arrow identifier
                        subject-grade
                        codomain-arg-grade
                        domain
                        codomain)
       -> {arg-grades : GradeVector} -> {arg : Term}
       -> Typing context-grades
                 arg-grades
                 domain-grades
                 context
                 arg
                 domain
       -> Typing context-grades
                 (vector-add function-grades
                             (vector-scale subject-grade arg-grades))
                 (vector-add codomain-context-grades
                             (vector-scale codomain-arg-grade arg-grades))
                 context
                 (apply function arg)
                 (substitute arg identifier codomain)
  pair : {context-grades : ContextGradeVector} -> {set-grades : GradeVector}
      -> {property-grades : GradeVector} -> {witness-grade : Grade}
      -> {context : Context} -> {identifier : Identifier} -> {set : Term}
      -> {property : Term} -> {set-level : Level}
      -> Typing (snoc context-grades set-grades)
                (snoc property-grades witness-grade)
                (snoc (zeros-like property-grades) none)
                (snoc context identifier set)
                property
                (type set-level)
      -> {witness-grades : GradeVector} -> {set-grades : GradeVector}
      -> {witness : Term}
      -> Typing context-grades witness-grades set-grades context witness set
      -> {proof-grades : GradeVector} -> {proof : Term}
      -> Typing context-grades
                proof-grades
                (vector-add property-grades
                            (vector-scale witness-grade witness-grades))
                context
                proof
                (substitute witness identifier property)
      -> Typing context-grades
                (vector-add witness-grades proof-grades)
                (vector-add set-grades property-grades)
                context
                (pair witness proof)
                (tensor identifier witness-grade set property)
  tensor-cut : {context-grades : ContextGradeVector}
            -> {exists-grades : GradeVector} -> {witness-grades : GradeVector}
            -> {property-grades : GradeVector} -> {context : Context}
            -> {exists : Term} -> {witness-binder : Identifier}
            -> {witness-grade : Grade} -> {set : Term} -> {property : Term}
            -> Typing context-grades
                      exists-grades
                      (vector-add witness-grades property-grades)
                      context
                      exists
                      (tensor witness-binder witness-grade set property)
            -> {class-grades : GradeVector} -> {class-exists-grade : Grade}
            -> {exists-binder : Identifier} -> {class : Term}
            -> {property-level : Level}
            -> Typing (snoc context-grades
                            (vector-add witness-grades property-grades))
                      (snoc class-grades class-exists-grade)
                      (snoc (zeros-like class-grades) none)
                      (snoc context
                            exists-binder
                            (tensor witness-binder witness-grade set property))
                      class
                      (type property-level)
            -> {body-grades : GradeVector} -> {body-exists-grade : Grade}
            -> {proof-binder : Identifier} -> {body : Term}
            -> Typing (snoc (snoc context-grades witness-grades)
                            (snoc property-grades witness-grade))
                      (snoc (snoc body-grades body-exists-grade)
                            body-exists-grade)
                      (snoc (snoc class-grades class-exists-grade)
                            class-exists-grade)
                      (snoc (snoc context witness-binder set)
                            proof-binder
                            property)
                      body
                      (substitute (pair (var witness-binder) (var proof-binder))
                                  exists-binder
                                  class)
            -> Typing context-grades
                      (vector-add body-grades
                                  (vector-scale body-exists-grade exists-grades))
                      (vector-add class-grades
                                  (vector-scale class-exists-grade exists-grades))
                      context
                      (unpair witness-binder proof-binder exists body)
                      (substitute exists exists-binder class)
  graded : {context-grades : ContextGradeVector} -> {set-grades : GradeVector}
        -> {context : Context} -> {set : Term} -> {level : Level}
        -> Typing context-grades
                  set-grades
                  (zeros-like set-grades)
                  context
                  set
                  (type level)
        -> (grade : Grade)
        -> Typing context-grades
                  set-grades
                  (zeros-like set-grades)
                  context
                  (graded grade set)
                  (type level)
  promote : {context-grades : ContextGradeVector}
         -> {promoted-grades : GradeVector} -> {set-grades : GradeVector}
         -> {context : Context} -> {promoted : Term} -> {set : Term}
         -> Typing context-grades
                   promoted-grades
                   set-grades
                   context
                   promoted
                   set
         -> (grade : Grade)
         -> Typing context-grades
                   (vector-scale grade promoted-grades)
                   set-grades
                   context
                   (promote promoted)
                   (graded grade set)
  demote : {context-grades : ContextGradeVector}
        -> {promoted-grades : GradeVector} -> {set-grades : GradeVector}
        -> {context : Context} -> {promoted : Term}
        -> {grade : Grade} -> {set : Term}
        -> Typing context-grades
                  promoted-grades
                  set-grades
                  context
                  promoted
                  (graded grade set)
        -> {result-grades : GradeVector} -> {promoted-grade : Grade}
        -> {promoted-binder : Identifier} -> {result : Term} -> {level : Level}
        -> Typing (snoc context-grades set-grades)
                  (snoc result-grades promoted-grade)
                  (snoc (zeros-like result-grades) none)
                  (snoc context promoted-binder (graded grade set))
                  result
                  (type level)
        -> {body-grades : GradeVector}
        -> {demoted-binder : Identifier} -> {body : Term}
        -> Typing (snoc context-grades set-grades)
                  (snoc body-grades grade)
                  (snoc result-grades (multiply grade promoted-grade))
                  (snoc context demoted-binder set)
                  body
                  (substitute (promote (var demoted-binder))
                              promoted-binder
                              result)
        -> Typing context-grades
                  (vector-add promoted-grades body-grades)
                  (vector-add result-grades
                              (vector-scale promoted-grade promoted-grades))
                  context
                  (demote demoted-binder promoted body)
                  (substitute promoted promoted-binder result)
  type-convert : {context-grades : ContextGradeVector}
              -> {term-grades : GradeVector} -> {type-grades : GradeVector}
              -> {context : Context} -> {term : Term} -> {subtype : Term}
              -> Typing context-grades
                        term-grades
                        type-grades
                        context
                        term
                        subtype
              -> {supertype : Term}
              -> Subtyping context-grades type-grades context subtype supertype
              -> Typing context-grades
                        term-grades
                        type-grades
                        context
                        term
                        supertype

data TermEquality where
  refl : {context-grades : ContextGradeVector}
      -> {term-grades : GradeVector} -> {type-grades : GradeVector}
      -> {context : Context} -> {term : Term} -> {type : Term}
      -> Typing context-grades term-grades type-grades context term type
      -> TermEquality context-grades
                      term-grades
                      type-grades
                      context
                      term
                      term
                      type
  trans : {context-grades : ContextGradeVector}
       -> {term-grades : GradeVector} -> {type-grades : GradeVector}
       -> {context : Context} -> {left : Term} -> {middle : Term}
       -> {type : Term}
       -> TermEquality context-grades
                       term-grades
                       type-grades
                       context
                       left
                       middle
                       type
       -> {right : Term}
       -> TermEquality context-grades
                       term-grades
                       type-grades
                       context
                       middle
                       right
                       type
       -> TermEquality context-grades
                       term-grades
                       type-grades
                       context
                       left
                       right
                       type
  sym : {context-grades : ContextGradeVector}
     -> {term-grades : GradeVector} -> {type-grades : GradeVector}
     -> {context : Context} -> {left : Term} -> {right : Term}
     -> {type : Term}
     -> TermEquality context-grades
                     term-grades
                     type-grades
                     context
                     left
                     right
                     type
     -> TermEquality context-grades
                     term-grades
                     type-grades
                     context
                     right
                     left
                     type
  conv-ty : {context-grades : ContextGradeVector}
         -> {term-grades : GradeVector} -> {type-grades : GradeVector}
         -> {context : Context} -> {left : Term} -> {right : Term}
         -> {subtype : Term}
         -> TermEquality context-grades
                         term-grades
                         type-grades
                         context
                         left
                         right
                         subtype
         -> {supertype : Term}
         -> Subtyping context-grades
                      type-grades
                      context
                      subtype
                      supertype
         -> TermEquality context-grades
                         term-grades
                         type-grades
                         context
                         left
                         right
                         supertype
  arrow : {context-grades : ContextGradeVector} -> {param-grades : GradeVector}
       -> {context : Context} -> {param-left : Term} -> {param-right : Term}
       -> {param-level : Level}
       -> TermEquality context-grades
                       param-grades
                       (zeros-like param-grades)
                       context
                       param-left
                       param-right
                       (type param-level)
       -> {result-grades : GradeVector} -> {param-grade : Grade}
       -> {identifier : Identifier}
       -> {result-left : Term} -> {result-right : Term}
       -> {result-level : Level}
       -> TermEquality (snoc context-grades param-grades)
                       (snoc result-grades param-grade)
                       (snoc (zeros-like result-grades) none)
                       (snoc context identifier param-left)
                       result-left
                       result-right
                       (type result-level)
       -> {body-grade : Grade}
       -> TermEquality context-grades
                       (vector-add param-grades result-grades)
                       (zeros-like param-grades)
                       context
                       (arrow identifier
                              body-grade
                              param-grade
                              param-left
                              result-left)
                       (arrow identifier
                              body-grade
                              param-grade
                              param-right
                              result-right)
                       (type (lub param-level result-level))

record Pair (L R : Set) : Set where
  constructor MkPair
  field
    fst : L
    snd : R

Environment : GradeVector -> Set
Environment empty = Unit
Environment (snoc init _) = Pair (Environment init) Term

evaluate : (term : Term)
        -> {context-grades : ContextGradeVector} -> {term-grades : GradeVector}
        -> {type-grades : GradeVector}
        -> {context : Context} -> {type : Term}
        -> Typing context-grades term-grades type-grades context term type
        -> Environment term-grades
        -> Term
evaluate (var x) (var empty empty (ext .x _) _) (MkPair _ val) = val
evaluate (var x) (var empty (snoc tail-context x₃ x₄) (ext .x₃ x₁) x₂) env =
  evaluate (var x) ? ?
evaluate (var x) (var (snoc tail-grades x₃) empty (ext .x x₁) x₂) (MkPair _ val) = val
evaluate (var x) (var (snoc tail-grades x₃) (snoc tail-context x₄ x₅) x₁ x₂) env = ?
evaluate (var x) (type-convert typing x₁) env = evaluate (var x) typing env
evaluate (type l) _ _ = type l
evaluate (arrow x s r param result) _ _ = arrow x s r param result
evaluate (lambda x term) _ _ = lambda x term
evaluate (apply (var x) arg) (apply typing fun-typing arg-typing) env = {!   !}
evaluate (apply (type x) arg) (apply typing (type-convert fun-typing x₁) arg-typing) env = {!   !}
evaluate (apply (arrow x x₁ x₂ fun fun₁) arg) (apply typing fun-typing arg-typing) env = {!   !}
evaluate (apply (lambda x fun) arg) (apply typing fun-typing arg-typing) env = {!   !}
evaluate (apply (apply fun fun₁) arg) (apply typing fun-typing arg-typing) env = {!   !}
evaluate (apply (tensor x x₁ fun fun₁) arg) (apply typing fun-typing arg-typing) env = {!   !}
evaluate (apply (pair fun fun₁) arg) (apply typing fun-typing arg-typing) env = {!   !}
evaluate (apply (unpair x x₁ fun fun₁) arg) (apply typing fun-typing arg-typing) env = {!   !}
evaluate (apply (graded x fun) arg) (apply typing fun-typing arg-typing) env = {!   !}
evaluate (apply (promote fun) arg) (apply typing fun-typing arg-typing) env = {!   !}
evaluate (apply (demote x fun fun₁) arg) (apply typing fun-typing arg-typing) env = {!   !}
evaluate (apply fun arg) (type-convert typing _) env =
  evaluate (apply fun arg) typing env
evaluate (tensor x s a b) typing env = tensor x s a b
evaluate (pair witness proof) (pair typing witness-typing proof-typing) env =
  pair (evaluate witness witness-typing ?) (evaluate proof proof-typing ?)
evaluate (pair witness proof) (type-convert typing x) env = {!   !}
evaluate (unpair x x₁ term term₁) typing env = {!   !}
evaluate (graded x term) typing env = {!   !}
evaluate (promote term) typing env = {!   !}
evaluate (demote x term term₁) typing env = {!   !}

-- (Strict) Values are closed in normal form

-- Lazy Values are a closure; a normal form, plus a "heap" containing the
-- closed over values (for each free variable), which may be lazy or strict.

-- NB: Strict values are a special case, since their normal form is closed,
-- their heap is empty.

-- A zero grade binding doesn't grow the heap at all.
-- A unit grade binding adds a strict value to the heap.
-- A ? grade binding adds a lazy value to the heap.
-- A * grade binding adds a lazy value to the heap.
-- A + grade binding adds a strict value to the heap.
