open import Agda.Builtin.Equality using (refl) renaming(_≡_ to Eq)

data Grade : Set where
  none : Grade
  one : Grade
  maybe : Grade -- 0 or 1; "?"
  ton : Grade -- 2 or more; "8"
  some : Grade -- 1 or more; "+"
  many : Grade -- 0 or more; "*"

add : Grade -> Grade -> Grade
add none g = g
add g none = g
add ton _ = ton
add _ ton = ton
add one one = ton
add one maybe = some
add one some = ton
add one many = some
add maybe one = some
add maybe maybe = many
add maybe some = some
add maybe many = many
add some one = ton
add some maybe = some
add some some = ton
add some many = some
add many one = some
add many maybe = many
add many some = some
add many many = many

join : Grade -> Grade -> Grade
join none none = none
join none one = maybe
join none maybe = maybe
join none ton = many
join none some = many
join none many = many
join one none = maybe
join one one = one
join one maybe = maybe
join one ton = some
join one some = some
join one many = many
join maybe none = maybe
join maybe one = maybe
join maybe maybe = maybe
join maybe ton = many
join maybe some = many
join maybe many = many
join ton none = many
join ton one = some
join ton maybe = many
join ton ton = ton
join ton some = some
join ton many = many
join some none = many
join some one = some
join some maybe = many
join some ton = some
join some some = some
join some many = many
join many none = many
join many one = many
join many maybe = many
join many ton = many
join many some = many
join many many = many

join-commutativity-l-r : (l : Grade) -> (r : Grade) -> Eq (join l r) (join r l)
join-commutativity-l-r none none = refl
join-commutativity-l-r none one = refl
join-commutativity-l-r none maybe = refl
join-commutativity-l-r none ton = refl
join-commutativity-l-r none some = refl
join-commutativity-l-r none many = refl
join-commutativity-l-r one none = refl
join-commutativity-l-r one one = refl
join-commutativity-l-r one maybe = refl
join-commutativity-l-r one ton = refl
join-commutativity-l-r one some = refl
join-commutativity-l-r one many = refl
join-commutativity-l-r maybe none = refl
join-commutativity-l-r maybe one = refl
join-commutativity-l-r maybe maybe = refl
join-commutativity-l-r maybe ton = refl
join-commutativity-l-r maybe some = refl
join-commutativity-l-r maybe many = refl
join-commutativity-l-r ton none = refl
join-commutativity-l-r ton one = refl
join-commutativity-l-r ton maybe = refl
join-commutativity-l-r ton ton = refl
join-commutativity-l-r ton some = refl
join-commutativity-l-r ton many = refl
join-commutativity-l-r some none = refl
join-commutativity-l-r some one = refl
join-commutativity-l-r some maybe = refl
join-commutativity-l-r some ton = refl
join-commutativity-l-r some some = refl
join-commutativity-l-r some many = refl
join-commutativity-l-r many none = refl
join-commutativity-l-r many one = refl
join-commutativity-l-r many maybe = refl
join-commutativity-l-r many ton = refl
join-commutativity-l-r many some = refl
join-commutativity-l-r many many = refl

multiply : Grade -> Grade -> Grade
multiply none _ = none
multiply _ none = none
multiply one y = y
multiply x one = x
multiply maybe maybe = maybe
multiply maybe ton = many
multiply maybe some = many
multiply maybe many = many
multiply ton maybe = many
multiply ton ton = ton
multiply ton some = ton
multiply ton many = many
multiply some maybe = many
multiply some ton = ton
multiply some some = some
multiply some many = many
multiply many maybe = many
multiply many ton = many
multiply many some = many
multiply many many = many

multiply-commutativity-l-r : (l : Grade)
                          -> (r : Grade)
                          -> Eq (multiply l r) (multiply r l)
multiply-commutativity-l-r none none = refl
multiply-commutativity-l-r none one = refl
multiply-commutativity-l-r none maybe = refl
multiply-commutativity-l-r none ton = refl
multiply-commutativity-l-r none some = refl
multiply-commutativity-l-r none many = refl
multiply-commutativity-l-r one none = refl
multiply-commutativity-l-r one one = refl
multiply-commutativity-l-r one maybe = refl
multiply-commutativity-l-r one ton = refl
multiply-commutativity-l-r one some = refl
multiply-commutativity-l-r one many = refl
multiply-commutativity-l-r maybe none = refl
multiply-commutativity-l-r maybe one = refl
multiply-commutativity-l-r maybe maybe = refl
multiply-commutativity-l-r maybe ton = refl
multiply-commutativity-l-r maybe some = refl
multiply-commutativity-l-r maybe many = refl
multiply-commutativity-l-r ton none = refl
multiply-commutativity-l-r ton one = refl
multiply-commutativity-l-r ton maybe = refl
multiply-commutativity-l-r ton ton = refl
multiply-commutativity-l-r ton some = refl
multiply-commutativity-l-r ton many = refl
multiply-commutativity-l-r some none = refl
multiply-commutativity-l-r some one = refl
multiply-commutativity-l-r some maybe = refl
multiply-commutativity-l-r some ton = refl
multiply-commutativity-l-r some some = refl
multiply-commutativity-l-r some many = refl
multiply-commutativity-l-r many none = refl
multiply-commutativity-l-r many one = refl
multiply-commutativity-l-r many maybe = refl
multiply-commutativity-l-r many ton = refl
multiply-commutativity-l-r many some = refl
multiply-commutativity-l-r many many = refl
